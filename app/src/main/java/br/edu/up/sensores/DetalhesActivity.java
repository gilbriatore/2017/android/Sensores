package br.edu.up.sensores;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetalhesActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detalhes);

    Intent intent = getIntent();
    final int tipoDoSensor = intent.getIntExtra("tipoDoSensor", 0);

    SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    Sensor sensor = sm.getDefaultSensor(tipoDoSensor);

    TextView txtNome = (TextView) findViewById(R.id.txtNome);
    TextView txtFaixa = (TextView) findViewById(R.id.txtFaixa);
    TextView txtDelay = (TextView) findViewById(R.id.txtDelay);
    TextView txtConsumo = (TextView) findViewById(R.id.txtConsumo);
    TextView txtFabricante = (TextView) findViewById(R.id.txtFabricante);
    TextView txtVersao = (TextView) findViewById(R.id.txtVersao);

    txtNome.setText(sensor.getName());
    txtFaixa.setText(String.valueOf(sensor.getMaximumRange()));
    txtDelay.setText(String.valueOf(sensor.getMinDelay()));
    txtConsumo.setText(String.valueOf(sensor.getPower()));
    txtFabricante.setText(sensor.getVendor());
    txtVersao.setText(String.valueOf(sensor.getVersion()));

    Button btnLer = (Button) findViewById(R.id.btnler);
    btnLer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getBaseContext(), ValoresActivity.class);
        intent.putExtra("tipoDoSensor", tipoDoSensor);
        startActivity(intent);
      }
    });
  }
}