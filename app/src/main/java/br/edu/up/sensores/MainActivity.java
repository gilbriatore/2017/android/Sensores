package br.edu.up.sensores;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    final List<Sensor> lista = sm.getSensorList(Sensor.TYPE_ALL);
    List<String> sensores = new ArrayList<>();

    for (Sensor sensor : lista) {
      sensores.add(sensor.getName());
    }

    ArrayAdapter<String> adapter = new ArrayAdapter<>(
        this, android.R.layout.simple_list_item_1, sensores);
    ListView listView = (ListView) findViewById(R.id.listView);
    listView.setAdapter(adapter);
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Sensor sensor = lista.get(position);
        Intent intent = new Intent(getBaseContext(), DetalhesActivity.class);
        intent.putExtra("tipoDoSensor", sensor.getType());
        startActivity(intent);
      }
    });
  }
}