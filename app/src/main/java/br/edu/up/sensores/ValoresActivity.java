package br.edu.up.sensores;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ValoresActivity extends AppCompatActivity
  implements SensorEventListener{

  private TextView txtV0;
  private TextView txtV1;
  private TextView txtV2;
  private TextView txtV3;
  private TextView txtV4;
  private TextView txtV5;
  private TextView txtV6;
  private TextView txtPrecisao;
  private TextView txtTempo;

  private SensorManager sm;
  private Sensor sensor;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_valores);

    Intent intent = getIntent();
    final int tipoDoSensor = intent.getIntExtra("tipoDoSensor", 0);

    sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensor = sm.getDefaultSensor(tipoDoSensor);

    TextView txtNome = (TextView) findViewById(R.id.txtNome);
    txtNome.setText(sensor.getName());

    txtV0 = (TextView) findViewById(R.id.txtV0);
    txtV1 = (TextView) findViewById(R.id.txtV1);
    txtV2 = (TextView) findViewById(R.id.txtV2);
    txtV3 = (TextView) findViewById(R.id.txtV3);
    txtV4 = (TextView) findViewById(R.id.txtV4);
    txtV5 = (TextView) findViewById(R.id.txtV5);
    txtV6 = (TextView) findViewById(R.id.txtV6);
    txtPrecisao = (TextView) findViewById(R.id.txtPrecisao);
    txtTempo = (TextView) findViewById(R.id.txtTempo);
  }

  @Override
  protected void onPause() {
    super.onPause();
    sm.unregisterListener(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
  }

  @Override
  public void onSensorChanged(SensorEvent event) {

    txtPrecisao.setText(String.valueOf(event.accuracy));
    txtTempo.setText(String.valueOf(event.timestamp));

    if (event.values.length > 0){
      txtV0.setText(String.valueOf(event.values[0]));
    } if (event.values.length > 1){
      txtV1.setText(String.valueOf(event.values[1]));
    } if (event.values.length > 2){
      txtV2.setText(String.valueOf(event.values[2]));
    } if (event.values.length > 3){
      txtV3.setText(String.valueOf(event.values[3]));
    } if (event.values.length > 4){
      txtV4.setText(String.valueOf(event.values[4]));
    } if (event.values.length > 5){
      txtV5.setText(String.valueOf(event.values[5]));
    } if (event.values.length > 6){
      txtV6.setText(String.valueOf(event.values[6]));
    }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
    //Sem implementação;
  }
}
